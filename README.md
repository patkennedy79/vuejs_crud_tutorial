## Synopsis

Tutorial of how to use VueJS to develop a basic CRUD (Create, Read, Update, Delete) website.

## Description

This tutorial provides an introduction to VueJS by building a website that manages a list of users. This type of site is called a CRUD (Create, Read, Update, and Delete) application, as it provides the key functionality for interacting with a database.

This tutorial is primarily focused on teaching the fundamentals of VueJS by building an application.  This application does NOT use the Vue CLI (Command Line Interface) tool, but focuses on VueJS fundamentals through a standard web page.

The styling of this application is done using CSS and CSS Grid. I’ll discuss a bit about the styling, but it is not the focus of the tutorial.  Additionally, the database that would typically be used for storing the data is being replaced by a web application (https://jsonplaceholder.typicode.com) which provides a REST API for testing applications.

## Example

![Alt text](/screenshots/VueJS_CRUD_Tutorial.gif?raw=true "VueJs CRUD Example")

## How to Run (Development)

Load the index.html file into your favorite web browser!

## Tutorials (based on git tags)

- 0.1 - Initial version with website layout using CSS Grid
- 0.2 - Adding VueJS to the website
- 0.3 - Displaying lists with v-for directive
- 0.4 - Using v-show and v-on directives
- 0.5 - Using v-if and v-else directives
- 0.6 - Methods (Part I)
- 0.7 - Methods (Part II)
- 0.8 - Style Binding
- 0.9 - Rending a List of Objects
- 0.10 - Debugging VueJS

- 1.0 - Components
- 1.1 - Components (Part II)
- 1.2 - VueJS Lifecycle
- 1.3 - Loading data via HTTP GET
- 1.4 - Error message banner
- 1.5 - Saving data via HTTP POST
- 1.6 - Deleting data via HTTP Delete
- 1.7 - Updating data via HTTP PUT
- 1.8 - Final project

## Additional Resources

I highly recommend the VueJS tutorial (https://www.youtube.com/playlist?list=PL4cUxeGkcC9gQcYgjhBoeQH7wiAyZNrYa) from the NetNinja on YouTube.  I think this is the best resource for learning VueJS!
